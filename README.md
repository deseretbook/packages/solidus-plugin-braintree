# solidus-plugin-braintree

A [solidus-sdk](https://solidus-sdk.netlify.com) plugin that adds support for the [solidus_braintree](https://github.com/solidusio/solidus_braintree) gem.

## Before You Get Started

You should have the [solidus_braintree](https://github.com/solidusio/solidus_braintree) gem installed
on your instance of Solidus already. If you don't, then get it setup first before you continue.

You should also have [solidus-sdk](https://solidus-sdk.netlify.com) installed and ready to go as well.

## Installation

```
yarn install solidus-plugin-braintree
```

Then whenever you initialize a new instance of the solidus-sdk, you can include the plugin like so:

```
import { Solidus } from 'solidus-sdk'
import solidusBraintreePlugin from 'solidus-plugin-braintree'

const instance = new Solidus({
  href: 'https://mysolidus.app',

  // Add the plugin here:
  plugins: [
    solidusBraintreePlugin,
  ],
})
```

## API

All examples below assume the instance of solidus-sdk was initialized as above:

### Generate a payment client token

`[POST] /api/payment_client_token`

Generates a new payment client token.

```
instance.payment().clientToken({ paymentMethodId: id })
```
