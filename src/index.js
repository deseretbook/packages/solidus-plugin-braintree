const plugin = {
  name: 'solidus-plugin-braintree',
  endpoints: {
    payment: {
      endpoints: {
        clientToken: {
          method: 'POST',
          path: '/api/payment_client_token',
        },
      },
    },
  },
}

export default plugin
